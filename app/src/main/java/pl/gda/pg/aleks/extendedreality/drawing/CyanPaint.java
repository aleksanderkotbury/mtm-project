package pl.gda.pg.aleks.extendedreality.drawing;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by aleks on 17.12.2016.
 */

public class CyanPaint extends Paint {

    public CyanPaint() {
        setColor(Color.CYAN);
        setStyle(Paint.Style.STROKE);
        setStrokeWidth(30);
    }
}
