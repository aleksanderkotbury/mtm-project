package pl.gda.pg.aleks.extendedreality.opencv;

import android.content.Context;
import android.util.Log;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;

/**
 * Created by aleks on 11.12.2016.
 */

public class OpenCVCameraLoader extends BaseLoaderCallback {

    private final JavaCameraView javaCameraView;

    public OpenCVCameraLoader(Context AppContext, JavaCameraView javaCameraView) {
        super(AppContext);
        this.javaCameraView = javaCameraView;
    }

    @Override
    public void onManagerConnected(int status) {
        switch (status) {
            case LoaderCallbackInterface.SUCCESS: {
                Log.i(getClass().getSimpleName(), "OpenCV loaded successfully");
                javaCameraView.enableView();
            }
            break;
            default: {
                super.onManagerConnected(status);
            }
            break;
        }
    }
}
