package pl.gda.pg.aleks.extendedreality.opengl;

import android.content.Context;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

/**
 * Created by aleks on 03.12.2016.
 */

public class OpenGlView extends GLSurfaceView {

    private final OpenGLRenderer renderer;

    public OpenGlView(Context context) {
        super(context);
        setEGLContextClientVersion(3);
        renderer = new OpenGLRenderer(this, context);
        setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        setPreserveEGLContextOnPause(true);
    }

    private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousX;
    private float mPreviousY;

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();

        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:

                float dx = x - mPreviousX;
                float dy = y - mPreviousY;

                float angle = renderer.getAngle() + ((dx + dy) * TOUCH_SCALE_FACTOR);
                renderer.setAngle((angle % 360));
                requestRender();
        }

        mPreviousX = x;
        mPreviousY = y;
        return true;
    }
}
