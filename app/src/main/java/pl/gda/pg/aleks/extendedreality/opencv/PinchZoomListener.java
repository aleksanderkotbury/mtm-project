package pl.gda.pg.aleks.extendedreality.opencv;

import android.util.Log;
import android.view.ScaleGestureDetector;

/**
 * Created by aleks on 16.12.2016.
 */

public class PinchZoomListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

    private Double scaleFactor = 2D;

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        synchronized (this) {
            float scaleFactor = detector.getScaleFactor();
            double factor = this.scaleFactor * scaleFactor * scaleFactor;
            this.scaleFactor = Math.max(1, Math.min(factor, 7));
        }

        return true;
    }

    public int getScaleFactor() {
        return (int) scaleFactor.doubleValue();
    }
}
