package pl.gda.pg.aleks.extendedreality.opencv;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.gda.pg.aleks.extendedreality.R;

public class OpenCVActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    static {
        OpenCVLoader.initDebug();
    }

    private BaseLoaderCallback mLoaderCallback;

    @BindView(R.id.camera_view)
    JavaCameraView javaCameraView;

    private int ratio = 1;
    private ScaleGestureDetector scaleGestureDetector;
    private PinchZoomListener listener;
    private CircleCacheMask circleCacheMask = new CircleCacheMask();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_open_cv);
        ButterKnife.bind(this);
        mLoaderCallback = getBaseLoaderCallback(javaCameraView);
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);

        listener = new PinchZoomListener();
        scaleGestureDetector = new ScaleGestureDetector(this, listener);
        getSupportActionBar().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        ratio = listener.getScaleFactor();
        Mat rgba = inputFrame.rgba();
        if (ratio > 1) {
            zoomImage(rgba);
        }
        return rgba;
    }

    private void zoomImage(Mat rgba) {
        Mat submat = getSubMat(rgba);
        int cols = rgba.cols();
        int rows = rgba.rows();
        Size size = new Size(cols / 2, rows / 2);
        Mat tempMat = new Mat();
        Imgproc.resize(submat, tempMat, size);
        Mat destMat = rgba.rowRange(rows / 4, rows * 3 / 4).colRange(cols / 4, cols * 3 / 4);
        Mat mask = circleCacheMask.findMask(destMat);
        tempMat.copyTo(destMat, mask);
    }

    private Mat getSubMat(Mat rgba) {
        int cols = rgba.cols();
        int rows = rgba.rows();
        int denominator = 4 * ratio;
        int nominatorStart = 1 + 2 * (ratio - 1);
        int nominatorEnd = 3 + 2 * (ratio - 1);
        int colsStart = cols * nominatorStart / denominator;
        int colsEnd = cols * nominatorEnd / denominator;
        int rowStart = rows * nominatorStart / denominator;
        int rowsEnd = rows * nominatorEnd / denominator;
        return rgba.submat(rowStart, rowsEnd, colsStart, colsEnd);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    private BaseLoaderCallback getBaseLoaderCallback(JavaCameraView javaCameraView) {
        return new OpenCVCameraLoader(this, javaCameraView);
    }
}
