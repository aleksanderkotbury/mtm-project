package pl.gda.pg.aleks.extendedreality.opengl.camera;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;


/**
 * Created by aleks on 10.12.2016.
 */

public class CameraGL {

    private final ByteBuffer mFullQuadVertices;
    private SurfaceTexture surfaceTexture;
    private int hProgram;
    private Camera camera;
    private int textureId = new CameraTexture().loadTexture();

    private static final byte FULL_QUAD_COORDS[] = {-1, 1, -1, -1, 1, 1, 1, -1};
    private float[] surfaceTransformMatrix = new float[16];
    private float[] cameraOrientation = new float[16];

    public CameraGL(SurfaceTexture.OnFrameAvailableListener onFrameAvailableListener) {
        mFullQuadVertices = ByteBuffer.allocateDirect(4 * 2);
        mFullQuadVertices.put(FULL_QUAD_COORDS).position(0);
        init(onFrameAvailableListener);
    }

    private void init(SurfaceTexture.OnFrameAvailableListener onFrameAvailableListener) {
        surfaceTexture = new SurfaceTexture(textureId);
        surfaceTexture.setOnFrameAvailableListener(onFrameAvailableListener);
        camera = new CameraInitializer().initCamera(surfaceTexture);

        GLES20.glClearColor(1.0f, 1.0f, 0.0f, 1.0f);

        hProgram = new CameraGLShader().loadShader();
        Matrix.setRotateM(cameraOrientation, 0, 0.0f, 0f, 0f, 1f);

    }

    public void draw() {
        synchronized (this) {
            surfaceTexture.updateTexImage();
        }
        GLES20.glUseProgram(hProgram);

        surfaceTexture.getTransformMatrix(surfaceTransformMatrix);

        int uTransformM = GLES20.glGetUniformLocation(hProgram, "uTransformM");
        int uOrientationM = GLES20.glGetUniformLocation(hProgram, "uOrientationM");

        GLES20.glUniformMatrix4fv(uTransformM, 1, false, surfaceTransformMatrix, 0);
        GLES20.glUniformMatrix4fv(uOrientationM, 1, false, cameraOrientation, 0);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId);

        int aPosition = GLES20.glGetAttribLocation(hProgram, "aPosition");
        GLES20.glVertexAttribPointer(aPosition, 2, GLES20.GL_BYTE, false, 0, mFullQuadVertices);
        GLES20.glEnableVertexAttribArray(aPosition);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }

    public void onChange() {
        camera.startPreview();
    }
}
