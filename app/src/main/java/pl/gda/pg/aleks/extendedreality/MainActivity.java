package pl.gda.pg.aleks.extendedreality;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.gda.pg.aleks.extendedreality.camera.CameraActivity;
import pl.gda.pg.aleks.extendedreality.detector.FaceDetectorActivity;
import pl.gda.pg.aleks.extendedreality.drawing.DrawingActivity;
import pl.gda.pg.aleks.extendedreality.opencv.OpenCVActivity;
import pl.gda.pg.aleks.extendedreality.opengl.OpenGLActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (checkPermission(Manifest.permission.CAMERA) || checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) || checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
    }

    private boolean checkPermission(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED;
    }

    @OnClick(R.id.open_gl_button)
    public void openGL() {
        Intent intent = new Intent(this, OpenGLActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.open_cv_button)
    public void openCV() {
        Intent intent = new Intent(this, OpenCVActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.drawing_button)
    public void drawing() {
        Intent intent = new Intent(this, DrawingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.gps_camera_button)
    public void gps() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.face_detector_button)
    public void faceDetector() {
        Intent intent = new Intent(this, FaceDetectorActivity.class);
        startActivity(intent);
    }
}
