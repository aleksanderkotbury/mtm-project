package pl.gda.pg.aleks.extendedreality.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

class CameraView extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera camera;

    public CameraView(Context context) {
        super(context);
        init();
    }

    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(getClass().getSimpleName(), "surface created");
        camera = Camera.open();
        try {
            setUpCamera();
            camera.setPreviewDisplay(holder);
            camera.startPreview();
            Log.d(getClass().getSimpleName(), "camera preview started");
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "sth went wrong", e);
        }
    }
    private void setUpCamera() {
        setUpCamera(0);
    }

    private void setUpCamera(int index) {
        List<Camera.Size> supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
        Camera.Size pictureSize = supportedPreviewSizes.get(index);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPictureSize(pictureSize.width, pictureSize.height);
        try {
            camera.setParameters(parameters);
        } catch (RuntimeException ex) {
            Log.w(getClass().getSimpleName(), "cannot setup supported camera preview size w:" + pictureSize.width + " h:" + pictureSize.height);
            setUpCamera(index+1);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.release();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        mHolder.setSizeFromLayout();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}