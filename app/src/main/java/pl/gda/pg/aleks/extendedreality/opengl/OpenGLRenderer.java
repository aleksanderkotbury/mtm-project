package pl.gda.pg.aleks.extendedreality.opengl;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import pl.gda.pg.aleks.extendedreality.opengl.camera.CameraFrameListener;
import pl.gda.pg.aleks.extendedreality.opengl.camera.CameraGL;
import pl.gda.pg.aleks.extendedreality.opengl.triangle.Triangle;
import pl.gda.pg.aleks.extendedreality.opengl.rotation.LastRotationMatrixProvider;

/**
 * Created by aleks on 03.12.2016.
 */

public class OpenGLRenderer implements GLSurfaceView.Renderer {

    private final GLSurfaceView glSurfaceView;
    private final LastRotationMatrixProvider lastRotationMatrixProvider;
    private Triangle triangle;
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];

    private float angle;
    private CameraGL cameraGL;

    public OpenGLRenderer(GLSurfaceView glSurfaceView, Context context) {
        this.glSurfaceView = glSurfaceView;
        lastRotationMatrixProvider = new LastRotationMatrixProvider(context);
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES30.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        triangle = new Triangle();
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        cameraGL = new CameraGL(new CameraFrameListener(glSurfaceView));
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT);
        float[] lastPosition = lastRotationMatrixProvider.getLastRotationMatrix();
        lastPosition[14] = -6;
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, lastPosition, 0);

        cameraGL.draw();
        triangle.draw(mMVPMatrix);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES30.glViewport(0, 0, width, height);
        float ratio = (float) width / height;
        cameraGL.onChange();

        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
    }

    float getAngle() {
        return angle;
    }

    void setAngle(float angle) {
        this.angle = angle;
    }
}
