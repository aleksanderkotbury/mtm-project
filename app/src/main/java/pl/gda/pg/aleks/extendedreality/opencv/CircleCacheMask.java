package pl.gda.pg.aleks.extendedreality.opencv;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by aleks on 17.12.2016.
 */

public class CircleCacheMask {

    private static final double[] ONE = new double[]{1};
    private static final double[] ZERO = new double[]{0};
    private Map<Size, Mat> cache = new HashMap<>();

    Mat findMask(Mat rectangleImage) {
        Size size = rectangleImage.size();
        Mat mask = cache.get(size);
        if (mask != null) {
            return mask;
        }

        Circle circle = findCircle(rectangleImage);
        Mat mat = new Mat(size, CvType.CV_8U, new Scalar(0));
        for (int x = 0; x < rectangleImage.cols(); x++) {
            for (int y = 0; y < rectangleImage.rows(); y++) {
                boolean b = circle.pointInCircle(x, y);
                if (b) {
                    mat.put(y, x, ONE);
                } else {
                    mat.put(y, x, ZERO);
                }
            }
        }

        cache.put(size, mat);
        return mat;
    }

    private Circle findCircle(Mat rectangleImage) {
        int radius = findRadius(rectangleImage);
        int rows = rectangleImage.rows();
        int cols = rectangleImage.cols();
        Point center = new Point(cols / 2, rows / 2);
        return new Circle(radius, center);
    }

    private int findRadius(Mat rectangleImage) {
        int rows = rectangleImage.rows();
        int cols = rectangleImage.cols();
        if (rows < cols) {
            return rows / 2;
        }

        return cols / 2;
    }
}
