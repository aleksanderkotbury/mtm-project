package pl.gda.pg.aleks.extendedreality.drawing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.view.View;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by aleks on 17.12.2016.
 */

public class DrawingView extends View implements View.OnTouchListener {

    private final Path path = new Path();
    private Paint paint;

    public DrawingView(Context context) {
        super(context);
        setOnTouchListener(this);
        paint = new CyanPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getPointerCount() > 1) {
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                path.moveTo(event.getX(), event.getY());
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                path.lineTo(event.getX(), event.getY());
                invalidate();
            }
        }

        return true;
    }
}
