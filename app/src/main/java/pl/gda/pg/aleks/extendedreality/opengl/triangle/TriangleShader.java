package pl.gda.pg.aleks.extendedreality.opengl.triangle;

import android.opengl.GLES20;

import pl.gda.pg.aleks.extendedreality.opengl.ShaderLoader;

/**
 * Created by aleks on 31.12.2016.
 */

public class TriangleShader {
    private final String vertexShaderCode =
            "uniform mat4 uMVPMatrix;" +
                    "varying vec4 v_Color;" +
                    "attribute vec4 a_Color;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "  v_Color = a_Color;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "varying vec4 v_Color;" +
                    "void main() {" +
                    "  gl_FragColor = v_Color;" +
                    "}";

    int loadTriangleProgram() {

        int vertexShader = ShaderLoader.loadVertexShader(vertexShaderCode);
        int fragmentShader = ShaderLoader.loadFragmentShader(fragmentShaderCode);

        int triangleProgram = GLES20.glCreateProgram();

        GLES20.glAttachShader(triangleProgram, vertexShader);
        GLES20.glAttachShader(triangleProgram, fragmentShader);
        GLES20.glLinkProgram(triangleProgram);
        return triangleProgram;
    }
}
