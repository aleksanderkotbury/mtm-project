package pl.gda.pg.aleks.extendedreality.opengl.camera;

import android.opengl.GLES20;
import android.util.Log;

import pl.gda.pg.aleks.extendedreality.opengl.ShaderLoader;

/**
 * Created by aleks on 10.12.2016.
 */

public class CameraGLShader {

    private final String vertexShader =
            "uniform mat4 uTransformM;\n" +
                    "uniform mat4 uOrientationM;" +
                    "attribute vec2 aPosition;" +
                    "varying vec2 vTextureCoord;" +
                    "void main(){" +
                    "  gl_Position = vec4(aPosition, 0.0, 1.0);" +
                    "  vTextureCoord = (uTransformM * ((uOrientationM * gl_Position + 1.0)*0.5)).xy;" +
                    "}";

    private final String frameShader =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;" +
                    "uniform samplerExternalOES sTexture;" +
                    "varying vec2 vTextureCoord;" +
                    "void main(){" +
                    "  gl_FragColor = texture2D(sTexture, vTextureCoord);" +
                    "}";


    int loadShader() {
        int vShared = ShaderLoader.loadVertexShader(vertexShader);
        int fShared = ShaderLoader.loadFragmentShader(frameShader);
        int program = GLES20.glCreateProgram();
        GLES20.glAttachShader(program, vShared);
        GLES20.glAttachShader(program, fShared);
        GLES20.glLinkProgram(program);
        return program;
    }
}
