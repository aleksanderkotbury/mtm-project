package pl.gda.pg.aleks.extendedreality.detector;

import android.content.Context;
import android.util.Log;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.objdetect.CascadeClassifier;

/**
 * Created by aleks on 11.12.2016.
 */

public class FaceDetectorLoader extends BaseLoaderCallback {

    private final FaceClassifierProvider faceClassifierProvider = new FaceClassifierProvider();
    private final JavaCameraView javaCameraView;
    private CascadeClassifier cascadeClassifier;

    public FaceDetectorLoader(Context AppContext, JavaCameraView javaCameraView) {
        super(AppContext);
        this.javaCameraView = javaCameraView;
    }

    @Override
    public void onManagerConnected(int status) {
        switch (status) {
            case LoaderCallbackInterface.SUCCESS: {
                Log.i(getClass().getSimpleName(), "OpenCV loaded successfully");
                loadFaceDetector();
                javaCameraView.enableView();
            }
            break;
            default: {
                super.onManagerConnected(status);
            }
            break;
        }
    }

    CascadeClassifier getCascadeClassifier() {
        return cascadeClassifier;
    }

    private void loadFaceDetector() {
        String faceDetectorClassifierPath = faceClassifierProvider.getFaceDetectorClassifierPath(mAppContext);
        cascadeClassifier = new CascadeClassifier(faceDetectorClassifierPath);
        boolean load = cascadeClassifier.load(faceDetectorClassifierPath);
        Log.i(getClass().getSimpleName(), "classifier loaded? " + load);
    }
}
