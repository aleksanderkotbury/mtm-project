package pl.gda.pg.aleks.extendedreality.opengl.rotation;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by aleks on 01.01.2017.
 */
public class RotationMatrixProvider {

    public Observable<float[]> getRotationMatrix(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        Sensor sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Sensor sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        int period = 100000;
        Observable<SensorEvent> magneticObservable = Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorMagnetic, period);
        });

        Observable<SensorEvent> gravityObservable = Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorGravity, period);
        });

        return Observable.zip(magneticObservable, gravityObservable, this::countRotation);
    }

    private float[] countRotation(SensorEvent magneticEvent, SensorEvent gravityEvent) {
        float[] rotation = new float[16];
        SensorManager.getRotationMatrix(rotation, null, gravityEvent.values, magneticEvent.values);
        return rotation;
    }
}
