package pl.gda.pg.aleks.extendedreality.opengl;

import android.opengl.GLES30;

public class ShaderLoader {

    public static int loadVertexShader(String shaderCode) {
        return loadShader(GLES30.GL_VERTEX_SHADER, shaderCode);
    }

    public static int loadFragmentShader(String shaderCode) {
        return loadShader(GLES30.GL_FRAGMENT_SHADER, shaderCode);
    }

    private static int loadShader(int type, String shaderCode) {
        int shader = GLES30.glCreateShader(type);

        GLES30.glShaderSource(shader, shaderCode);
        GLES30.glCompileShader(shader);

        return shader;
    }
}
