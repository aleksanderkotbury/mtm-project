package pl.gda.pg.aleks.extendedreality.camera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import org.reactivestreams.Subscriber;

import io.reactivex.Observable;

/**
 * Created by aleks on 06.01.2017.
 */

public class LocationDataProvider {

    public Observable<Location> getLocationUpdates(Context context) {
        return Observable.fromPublisher(publisher -> listenForGPS(context, publisher));
    }

    private void listenForGPS(Context context, Subscriber<? super Location> publisher) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            publisher.onError(new RuntimeException("app does not have permission to use gps"));
        }
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        RxLocationListener rxLocationListener = new RxLocationListener(publisher);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, rxLocationListener);
    }
}
