package pl.gda.pg.aleks.extendedreality.camera;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import java.util.logging.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.gda.pg.aleks.extendedreality.R;

public class CameraActivity extends AppCompatActivity {

    @BindView(R.id.activity_camera)
    RelativeLayout relativeLayout;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
        CameraView cameraView = new CameraView(this);
        relativeLayout.addView(cameraView);
        GpsView gpsView = new GpsView(this);
        relativeLayout.addView(gpsView);
        new LocationDataProvider().getLocationUpdates(this)
//                .doOnNext(location -> Log.i(getClass().getSimpleName(), location.toString()))
                .subscribe(gpsView::setLocation);
        getSupportActionBar().hide();
    }
}
