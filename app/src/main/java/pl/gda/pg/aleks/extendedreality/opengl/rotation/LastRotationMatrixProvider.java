package pl.gda.pg.aleks.extendedreality.opengl.rotation;

import android.content.Context;
import android.util.Log;

/**
 * Created by aleks on 02.01.2017.
 */

public class LastRotationMatrixProvider {

    private final ArrayWrapper arrayWrapper;

    public LastRotationMatrixProvider(Context context) {
        arrayWrapper = new ArrayWrapper();
        new RotationMatrixProvider()
                .getRotationMatrix(context)
                .subscribe(arrayWrapper::setArray, t -> Log.e(getClass().getSimpleName(), "error: ", t));
    }

    public float[] getLastRotationMatrix() {
        return arrayWrapper.getArray();
    }
}
