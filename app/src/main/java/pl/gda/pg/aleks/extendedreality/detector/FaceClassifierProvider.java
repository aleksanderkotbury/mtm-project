package pl.gda.pg.aleks.extendedreality.detector;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import pl.gda.pg.aleks.extendedreality.R;

/**
 * Created by aleks on 06.01.2017.
 */

public class FaceClassifierProvider {

    public String getFaceDetectorClassifierPath(Context context) {
        File detectorDir = context.getDir("detector", Context.MODE_PRIVATE);
        File classifierFile = new File(detectorDir, "face_detector_classifier.xml");
        if (classifierFile.exists()) {
            String absolutePath = classifierFile.getAbsolutePath();
            Log.i(getClass().getSimpleName(), "providing existing face detector file path: " + absolutePath);
            return absolutePath;
        }

        copyResourceToFile(context, classifierFile);
        return classifierFile.getAbsolutePath();
    }

    private void copyResourceToFile(Context context, File classifierFile) {
        try (FileOutputStream os = new FileOutputStream(classifierFile); InputStream is = context.getResources().openRawResource(R.raw.face_detector_classifier)) {
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (IOException ex) {
            Log.e(getClass().getSimpleName(), "cannot copy resource to file", ex);
            throw new RuntimeException("Cannot copy resource to file!!", ex);
        }
    }
}
