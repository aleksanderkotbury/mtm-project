package pl.gda.pg.aleks.extendedreality.opengl.camera;

import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;

/**
 * Created by aleks on 01.01.2017.
 */

public class CameraFrameListener implements SurfaceTexture.OnFrameAvailableListener {
    private final GLSurfaceView glSurfaceView;

    public CameraFrameListener(GLSurfaceView glSurfaceView) {
        this.glSurfaceView = glSurfaceView;
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        glSurfaceView.requestRender();
    }
}
