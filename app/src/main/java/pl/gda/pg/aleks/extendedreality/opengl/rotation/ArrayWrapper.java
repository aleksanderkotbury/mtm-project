package pl.gda.pg.aleks.extendedreality.opengl.rotation;

/**
 * Created by aleks on 02.01.2017.
 */

public class ArrayWrapper {
    private float[] position = new float[3];

    public float[] getArray() {
        return position;
    }

    public void setArray(float[] position) {
        this.position = position;
    }
}
