package pl.gda.pg.aleks.extendedreality.camera;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import org.reactivestreams.Subscriber;

/**
 * Created by aleks on 06.01.2017.
 */

public class RxLocationListener implements LocationListener {

    private final Subscriber<? super Location> publisher;

    public RxLocationListener(Subscriber<? super Location> publisher) {
        this.publisher = publisher;
    }

    @Override
    public void onLocationChanged(Location location) {
//        Log.i(getClass().getSimpleName(), "location received " + location.toString());
        publisher.onNext(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
