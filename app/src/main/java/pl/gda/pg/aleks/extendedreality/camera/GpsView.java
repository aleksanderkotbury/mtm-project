package pl.gda.pg.aleks.extendedreality.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.text.DecimalFormat;
import java.util.Locale;

public class GpsView extends View {

    private Location location = new Location("null provider");
    private float speed = 0;
    private Paint paint = new Paint();
    DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public GpsView(Context context) {
        super(context);
        initColor();
    }

    public GpsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initColor();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Location safeLocation;
        synchronized (location) {
            safeLocation = location;
        }

        String location = String.format(Locale.ENGLISH, "%f, %f", safeLocation.getLongitude(), safeLocation.getLatitude());
        canvas.drawText(location, 0, 100, paint);
        int height = getHeight();
        String speed = String.format(Locale.ENGLISH, "Speed: %.2f", this.speed);
        canvas.drawText(speed, 0, height, paint);
    }

    public void setLocation(Location location) {
        synchronized (location) {
            float distance = this.location.distanceTo(location);
            long newTime = location.getElapsedRealtimeNanos();
            long oldTime = this.location.getElapsedRealtimeNanos();
            long time = newTime - oldTime;
            double seconds = time / 1_000_000_000F;
            speed = (float) (distance / seconds);
            this.location = location;
        }
        invalidate();
    }

    private void initColor() {
        paint.setColor(Color.CYAN);
        paint.setTextSize(100);
    }
}
