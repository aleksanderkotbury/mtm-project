package pl.gda.pg.aleks.extendedreality.opengl.triangle;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import pl.gda.pg.aleks.extendedreality.opengl.ShaderLoader;

/**
 * Created by aleks on 03.12.2016.
 */
public class Triangle {

    private final FloatBuffer colorBuffer;

    private FloatBuffer vertexBuffer;

    private static float TRIANGLE_COORDS[] = {   // in counterclockwise order:
            0.0f, 0.622008459f, 0.0f, // top
            -0.5f, -0.311004243f, 0.5f, // bottom left
            0.5f, -0.311004243f, 0.5f, // bottom right
            0.0f, -0.311004243f, -0.5f   // bottom behind
    };

    private static final int COORDS_PER_VERTEX = 3;
    private static final int VERTEX_STRIDE = COORDS_PER_VERTEX * 4;

    private float[] colors = {
            1f, 0f, 0f, 0.5f, // vertex 0 red
            0f, 1f, 0f, 0.5f, // vertex 1 green
            0f, 0f, 1f, 0.5f, // vertex 2 blue
            1f, 0f, 1f, 0.5f // vertex 3 magenta
    };

    private short drawOrder[] = {
            0, 1, 2,
            0, 2, 3,
            3, 1, 0,
            3, 2, 1
    }; // order to draw vertices
    private final int triangleProgram;
    private final ShortBuffer drawListBuffer;

    public Triangle() {
        ByteBuffer bb = ByteBuffer.allocateDirect(
                TRIANGLE_COORDS.length * 4);
        bb.order(ByteOrder.nativeOrder());

        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(TRIANGLE_COORDS);
        vertexBuffer.position(0);

        triangleProgram = new TriangleShader().loadTriangleProgram();

        ByteBuffer dlb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 2 bytes per short)
                drawOrder.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);

        ByteBuffer cb = ByteBuffer.allocateDirect(colors.length * 4);
        cb.order(ByteOrder.nativeOrder());
        colorBuffer = cb.asFloatBuffer();
        colorBuffer.put(colors);
        colorBuffer.position(0);
    }

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(triangleProgram);

        int trianglePosition = GLES20.glGetAttribLocation(triangleProgram, "vPosition");
        GLES20.glEnableVertexAttribArray(trianglePosition);
        GLES20.glVertexAttribPointer(trianglePosition, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                VERTEX_STRIDE, vertexBuffer);

        int triangleColor = GLES20.glGetAttribLocation(triangleProgram, "a_Color");
        GLES20.glEnableVertexAttribArray(triangleColor);
        GLES20.glVertexAttribPointer(triangleColor, 4, GLES20.GL_FLOAT, false, 0, colorBuffer);

        int mMVPMatrixHandle = GLES20.glGetUniformLocation(triangleProgram, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length,
                GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        GLES20.glDisableVertexAttribArray(triangleColor);
        GLES20.glDisableVertexAttribArray(trianglePosition);
    }
}