package pl.gda.pg.aleks.extendedreality.drawing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.gda.pg.aleks.extendedreality.R;

public class DrawingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DrawingView drawingView = new DrawingView(this);
        setContentView(drawingView);
    }
}
