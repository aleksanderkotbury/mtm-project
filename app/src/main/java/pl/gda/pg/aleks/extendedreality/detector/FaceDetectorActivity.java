package pl.gda.pg.aleks.extendedreality.detector;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.gda.pg.aleks.extendedreality.R;

public class FaceDetectorActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final Scalar COLOR = new Scalar(128, 128, 0, 255);
    public static final Size MIN_SIZE = new Size(150, 150);
    public static final Size MAX_SIZE = new Size();
    @BindView(R.id.camera_view)
    JavaCameraView javaCameraView;

    private FaceDetectorLoader faceDetectorLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_open_cv);
        ButterKnife.bind(this);
        faceDetectorLoader = getBaseLoaderCallback(javaCameraView);
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);

        getSupportActionBar().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, faceDetectorLoader);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat gray = inputFrame.gray();

        MatOfRect faces = new MatOfRect();
        faceDetectorLoader.getCascadeClassifier().detectMultiScale(gray, faces, 1.1, 2, 2,
                MIN_SIZE, MAX_SIZE);
        Mat rgba = inputFrame.rgba();

        for(Rect face: faces.toArray()) {
            Imgproc.rectangle(rgba, face.tl(), face.br(), COLOR, 3);
        }
        return rgba;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (javaCameraView != null) {
            javaCameraView.disableView();
        }
    }

    private FaceDetectorLoader getBaseLoaderCallback(JavaCameraView javaCameraView) {
        return new FaceDetectorLoader(this, javaCameraView);
    }
}
