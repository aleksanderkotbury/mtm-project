package pl.gda.pg.aleks.extendedreality.opencv;

import org.opencv.core.Point;

/**
 * Created by aleks on 17.12.2016.
 */

public class Circle {

    private final int radius;
    private final Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public boolean pointInCircle(int x, int y) {
        double xPow = (x - center.x) * (x - center.x);
        double yPow = (y - center.y) * (y - center.y);
        double dPow = xPow + yPow;
        double radiusPow = radius * radius;
        return dPow <= radiusPow;
    }
}
