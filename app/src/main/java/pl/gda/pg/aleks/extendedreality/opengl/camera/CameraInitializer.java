package pl.gda.pg.aleks.extendedreality.opengl.camera;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.util.Log;

import java.io.IOException;
import java.util.List;

/**
 * Created by aleks on 10.12.2016.
 */

public class CameraInitializer {

    Camera initCamera(SurfaceTexture surfaceTexture) {
        Camera camera = Camera.open();
        List<Camera.Size> supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
        Camera.Size pictureSize = supportedPreviewSizes.get(0);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPreviewSize(pictureSize.width, pictureSize.height);

        camera.setDisplayOrientation(90);
        camera.setParameters(parameters);

        try {
            camera.setPreviewTexture(surfaceTexture);
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "Cannot set preview texure for camera. Reason: ", e);
        }
//            float horizontalViewAngle = camera.getParameters().getHorizontalViewAngle();
//            float verticalViewAngle = camera.getParameters().getVerticalViewAngle();
//            Log.i(getClass().getSimpleName(), pictureSize.height + " " + pictureSize.width + " " + horizontalViewAngle + " " + verticalViewAngle);
        return camera;
    }
}
