package pl.gda.pg.aleks.extendedreality.opengl.rotation;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import io.reactivex.Observable;

/**
 * Created by aleks on 03.01.2017.
 */
public class AccelerometerDataProvider {
    private final ArrayWrapper arrayWrapper = new ArrayWrapper();

    public AccelerometerDataProvider(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        Observable.<SensorEvent>fromPublisher(publisher -> {
            sensorManager.registerListener(new SensorEventPublisher(publisher), sensorAccelerometer, 100000);
        }).subscribe(event -> {
            arrayWrapper.setArray(event.values);
        });
    }

    public float[] lastAccelerometerData() {
        return arrayWrapper.getArray();
    }
}
